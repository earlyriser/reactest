describe('My First Test', () => {
  it('Visits the todo', () => {
    cy.visit('http://localhost:3000/todoapp');
    cy.get('button').contains('Add Todo');
  });
  it('Contains an Add Todo', () => {
    cy.get('button').contains('Add Todo');
  });
  it('Adds the tasks to the list', () => {
    cy.get('input').type('Learn JS');
    cy.get('button').click();
    cy.get('input').type('Learn React');
    cy.get('button').click();

    cy.get('.todoList li').first().contains('Learn JS');
    cy.get('.todoList li').last().contains('Learn React');
  });
});

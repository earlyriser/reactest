import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom';
import './App.css';
import Home from './components/home/Home';
import Dashboard from './components/dashboard/Dashboard';
import Todoapp from './components/todoapp/Todoapp';

import AddTodo from './containers/AddTodo';
import VisibleTodoList from './containers/VisibleTodoList';


const App = () => (
  <div className="App">
    <Router>
      <div>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/dashboard">Dashboard</Link></li>
          <li><Link to="/todoapp">Redux todos</Link></li>
        </ul>
        <hr />
        <Route exact path="/" component={Home} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/todoapp" component={Todoapp} />
      </div>
    </Router>

  </div>
);


export default App;

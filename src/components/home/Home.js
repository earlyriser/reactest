import React from 'react';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';
import styled from 'styled-components';
import logo from '../../logo.svg';

const Title = styled.h1`
  color: palevioletred;
`;

const Home = () => (
  <div>
    <Title>
      <FormattedHTMLMessage id="app.styled" />
    </Title>
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <h1 className="App-title">
        <FormattedMessage
          id="app.title"
          defaultMessage="Welcome to {what}"
          description="Header on app main page"
          values={{ what: 'React-intl' }}
        />
      </h1>
    </header>
    <FormattedHTMLMessage
      id="app.intro"
      defaultMessage="Change your browser language config to see it in action."
      description="Text on main page"
    />
  </div>
);

export default Home;

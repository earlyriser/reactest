import React from 'react';
import ReactDOM from 'react-dom';
import DashboardSidebar from './DashboardSidebar';


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DashboardSidebar />, div);
});

import React from 'react';
import Moto7 from '@haiku/rmartinezlws-moto7/react';

const DashboardContent = () => (
  <div className="">
    <Moto7 haikuOptions={{loop: true, contextMenu: 'disabled',}} />
  </div>
);

export default DashboardContent;

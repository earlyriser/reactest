import React from 'react';
import axios from 'axios';
import Badges from '../../components/badges/Badges';
import DashboardContent from '../../components/dashboardContent/DashboardContent';
import DashboardSidebar from '../../components/dashboardSidebar/DashboardSidebar';



function foo (){
  axios.get('https://jsonplaceholder.typicode.com/posts')
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
}


foo();

const Dashboard = () => (
  <div className="">
    <ul>
      <li>The badges are getting dynamic properties</li>
      <li>The animation below is fully SVG: small size, no pixelization and it
        could react to events like clicks, we are using Haiku to create it.
      </li>
    </ul>
    <Badges />
    <DashboardContent />
    <DashboardSidebar />
  </div>
);

export default Dashboard;

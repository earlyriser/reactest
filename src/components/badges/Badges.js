import React from 'react';
import Badge from '../../components/badge/Badge';

const Badges = () => (
  <div className="">
    <Badge name="Kill spiders with water" element="water" />
    <Badge name="Kill spiders with helium" element="helium" />
    <Badge name="Kill spiders with fire" element="fire" />
  </div>
);

export default Badges;

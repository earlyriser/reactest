import React from 'react';
import { configure, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Badge from './Badge';

configure({ adapter: new Adapter() });

it('It shows the prop name as text', () => {
  const wrapper = render(<Badge name="Kill spiders with juice" element="water" />);
  expect(wrapper.text()).toEqual('Kill spiders with juice');
});

import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const BadgeContainer = styled.div`
  height: 100px;
  width:100px;
  border-radius:50%;
  background-color: gold;
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: 0px -10px 7px rgba(255,0,0,0.3), 0px 10px 7px rgba(0,0,255,0.3);
  top:0px;
  :hover{
    background-color: #AD310B;
    transition: background-color 1000ms linear;
    cursor:pointer;
    top:10px;
  }
`;

const BadgeContent = styled.div`
  color: orange;
  text-transform: uppercase;
  font-size: 13px;
  font-weight: bold;
`;

const shades = {
  water: { color: 'blue' },
  fire: { color: 'red' },
};

const Badge = ({ name, element }) => (
  <div className="Badge">
    <BadgeContainer>
      <BadgeContent style={shades[element]}>
        {name}
      </BadgeContent>
    </BadgeContainer>
  </div>
);

Badge.propTypes = {
  name: PropTypes.string.isRequired,
  element: PropTypes.string.isRequired,
};

export default Badge;

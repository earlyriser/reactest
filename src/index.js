import React from 'react';
import ReactDOM from 'react-dom';
import { IntlProvider, addLocaleData } from 'react-intl';
import localeEn from 'react-intl/locale-data/en';
import localeFr from 'react-intl/locale-data/fr';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import messagesFr from './translations/fr.json';
import messagesEn from './translations/en.json';

const store = createStore(rootReducer, applyMiddleware(thunk));

addLocaleData([...localeEn, ...localeFr]);

const messages = {
  fr: messagesFr,
  en: messagesEn,
};

const language = navigator.language.split(/[-_]/)[0];

ReactDOM.render(<Provider store={store}><IntlProvider locale={language} messages={messages[language]}><App /></IntlProvider></Provider>, document.getElementById('root'));
registerServiceWorker();
